CREATE DATABASE  IF NOT EXISTS `analisismedios` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `analisismedios`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: analisismedios
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administracion_proyecto`
--

DROP TABLE IF EXISTS `administracion_proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administracion_proyecto` (
  `fk_id_proyecto` int(11) NOT NULL,
  `fk_id_usuario` int(11) NOT NULL,
  `perfil` int(11) NOT NULL DEFAULT '1',
  KEY `fk_id_proyecto_administracion_idx` (`fk_id_proyecto`),
  KEY `fk_id_usuario_administracion_idx` (`fk_id_usuario`),
  CONSTRAINT `fk_id_proyecto_administracion` FOREIGN KEY (`fk_id_proyecto`) REFERENCES `proyecto` (`id_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_usuario_administracion` FOREIGN KEY (`fk_id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administracion_proyecto`
--

LOCK TABLES `administracion_proyecto` WRITE;
/*!40000 ALTER TABLE `administracion_proyecto` DISABLE KEYS */;
INSERT INTO `administracion_proyecto` VALUES (4,1,1);
/*!40000 ALTER TABLE `administracion_proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conflicto`
--

DROP TABLE IF EXISTS `conflicto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conflicto` (
  `id_conflicto` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id_conflicto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conflicto`
--

LOCK TABLES `conflicto` WRITE;
/*!40000 ALTER TABLE `conflicto` DISABLE KEYS */;
/*!40000 ALTER TABLE `conflicto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medio`
--

DROP TABLE IF EXISTS `medio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medio` (
  `id_medio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_medio`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medio`
--

LOCK TABLES `medio` WRITE;
/*!40000 ALTER TABLE `medio` DISABLE KEYS */;
INSERT INTO `medio` VALUES (1,'MVC Noticias'),(2,'Televisa News'),(3,'Indefinido'),(4,'Gonzalo Muro');
/*!40000 ALTER TABLE `medio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medio_reportero`
--

DROP TABLE IF EXISTS `medio_reportero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medio_reportero` (
  `fk_id_medio` int(11) NOT NULL,
  `fk_id_repotero` int(11) NOT NULL,
  KEY `fk_id_medio_reportero_idx` (`fk_id_medio`),
  KEY `fk_id_reportero_medio_idx` (`fk_id_repotero`),
  CONSTRAINT `fk_id_medio_reportero` FOREIGN KEY (`fk_id_medio`) REFERENCES `medio` (`id_medio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_reportero_medio` FOREIGN KEY (`fk_id_repotero`) REFERENCES `reportero` (`id_reportero`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medio_reportero`
--

LOCK TABLES `medio_reportero` WRITE;
/*!40000 ALTER TABLE `medio_reportero` DISABLE KEYS */;
/*!40000 ALTER TABLE `medio_reportero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota`
--

DROP TABLE IF EXISTS `nota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota` (
  `id_nota` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_archivo` varchar(100) NOT NULL,
  `clasificacion` varchar(100) DEFAULT NULL,
  `encabezado` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `url_nota` varchar(255) DEFAULT NULL,
  `tendencia` tinyint(4) DEFAULT NULL,
  `fk_id_medio_emitio` int(11) DEFAULT NULL,
  `fk_id_reportero_firmo` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `fk_id_usuario_alta` int(11) DEFAULT NULL,
  `estatus` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id_nota`),
  KEY `fk_id_medio_emitio_idx` (`fk_id_medio_emitio`),
  KEY `fk_id_usuario_alta_idx` (`fk_id_usuario_alta`),
  CONSTRAINT `fk_id_medio_emitio` FOREIGN KEY (`fk_id_medio_emitio`) REFERENCES `medio` (`id_medio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_usuario_alta` FOREIGN KEY (`fk_id_usuario_alta`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota`
--

LOCK TABLES `nota` WRITE;
/*!40000 ALTER TABLE `nota` DISABLE KEYS */;
INSERT INTO `nota` VALUES (41,'e965846c4ad89ae9eed224d2d9bffea4.jpg',NULL,'Prueba de encabezado 2','2017-04-25','http://www.google.com',1,1,1,NULL,NULL,-1),(42,'c16ec0bad57275429983eff16485751c.pdf',NULL,'Temario: Programación Web','2017-04-24','Instructor: LSCA José Gonzalo Muro Polanco',0,1,2,NULL,NULL,-1),(43,'e6922eafd05faf9085b3569dbf738f5d.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-1),(44,'77e977e1ca3e4d11d1c03ea5c8b52a71.pdf',NULL,'EPN garantiza a Yunes apoyo para devolver la seguridad a Veracruz','2017-03-02','https://facebook.github.io/react/docs/refs-and-the-dom.html',-1,3,2,NULL,NULL,-1),(45,'a25da73e61a5100f870d47aa14d2738f.pdf',NULL,'xss','2017-04-25','wsxw',0,4,4,NULL,NULL,1),(46,'e342f1d315717fff7c20c4d94d8f9a60.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(47,'e720620717ebcef727010542f19f00be.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(48,'ae406187a1c66f3a6ac31f56af519cd9.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(49,'375514a2051f9df6e34fbeb9e681e167.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(50,'108ca433a3e3d3aaba04f4cda8ba5aee.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(51,'b6378389484948e9b37f811495bf770b.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(52,'9b01b8f6146ebb2609bef8d6b09baeaa.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(53,'4952da26000d6437bc599e198c44ff9c.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(54,'f760841c6074071370c7454c450fb61b.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `nota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota_conflicto`
--

DROP TABLE IF EXISTS `nota_conflicto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_conflicto` (
  `fk_id_nota` int(11) NOT NULL,
  `fk_id_conflicto` int(11) NOT NULL,
  KEY `fk_id_nota_conflicto_idx` (`fk_id_nota`),
  KEY `fk_id_conflicto_nota_idx` (`fk_id_conflicto`),
  CONSTRAINT `fk_id_conflicto_nota` FOREIGN KEY (`fk_id_conflicto`) REFERENCES `conflicto` (`id_conflicto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_nota_conflicto` FOREIGN KEY (`fk_id_nota`) REFERENCES `nota` (`id_nota`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota_conflicto`
--

LOCK TABLES `nota_conflicto` WRITE;
/*!40000 ALTER TABLE `nota_conflicto` DISABLE KEYS */;
/*!40000 ALTER TABLE `nota_conflicto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota_personaje`
--

DROP TABLE IF EXISTS `nota_personaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_personaje` (
  `fk_id_nota` int(11) NOT NULL,
  `fk_id_personaje` int(11) NOT NULL,
  `fk_id_organizacion` int(11) NOT NULL,
  `tendencia` tinyint(4) NOT NULL,
  KEY `fk_id_nota_personaje_idx` (`fk_id_nota`),
  KEY `fk_id_personaje_nota_idx` (`fk_id_personaje`),
  KEY `fk_id_organizacion_nota_idx` (`fk_id_organizacion`),
  CONSTRAINT `fk_id_nota_personaje` FOREIGN KEY (`fk_id_nota`) REFERENCES `nota` (`id_nota`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_organizacion_nota` FOREIGN KEY (`fk_id_organizacion`) REFERENCES `organizacion` (`id_organizacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_personaje_nota` FOREIGN KEY (`fk_id_personaje`) REFERENCES `personaje` (`id_personaje`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota_personaje`
--

LOCK TABLES `nota_personaje` WRITE;
/*!40000 ALTER TABLE `nota_personaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `nota_personaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota_tema`
--

DROP TABLE IF EXISTS `nota_tema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_tema` (
  `fk_id_nota` int(11) NOT NULL AUTO_INCREMENT,
  `fk_id_tema` int(11) DEFAULT NULL,
  `extracto` text,
  PRIMARY KEY (`fk_id_nota`),
  KEY `fk_id_tema_idx` (`fk_id_tema`),
  CONSTRAINT `fk_id_nota` FOREIGN KEY (`fk_id_nota`) REFERENCES `nota` (`id_nota`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_tema` FOREIGN KEY (`fk_id_tema`) REFERENCES `tema` (`id_tema`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota_tema`
--

LOCK TABLES `nota_tema` WRITE;
/*!40000 ALTER TABLE `nota_tema` DISABLE KEYS */;
/*!40000 ALTER TABLE `nota_tema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizacion`
--

DROP TABLE IF EXISTS `organizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizacion` (
  `id_organizacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_organizacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizacion`
--

LOCK TABLES `organizacion` WRITE;
/*!40000 ALTER TABLE `organizacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizacion_personaje`
--

DROP TABLE IF EXISTS `organizacion_personaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizacion_personaje` (
  `fk_id_organizacion` int(11) NOT NULL,
  `fk_id_personaje` int(11) NOT NULL,
  KEY `fk_ir_organizacion_personaje_idx` (`fk_id_organizacion`),
  KEY `fk_id_personaje_organizacion_idx` (`fk_id_personaje`),
  CONSTRAINT `fk_id_organizacion_personaje` FOREIGN KEY (`fk_id_organizacion`) REFERENCES `organizacion` (`id_organizacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_personaje_organizacion` FOREIGN KEY (`fk_id_personaje`) REFERENCES `personaje` (`id_personaje`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizacion_personaje`
--

LOCK TABLES `organizacion_personaje` WRITE;
/*!40000 ALTER TABLE `organizacion_personaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizacion_personaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personaje`
--

DROP TABLE IF EXISTS `personaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personaje` (
  `id_personaje` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_personaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personaje`
--

LOCK TABLES `personaje` WRITE;
/*!40000 ALTER TABLE `personaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `personaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `id_proyecto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_corto` varchar(45) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `fk_id_usuario_creacion` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `estatus` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id_proyecto`),
  UNIQUE KEY `nombre_corto_UNIQUE` (`nombre_corto`),
  KEY `fk_id_usuario_creacion_proyecto_idx` (`fk_id_usuario_creacion`),
  CONSTRAINT `fk_id_usuario_creacion_proyecto` FOREIGN KEY (`fk_id_usuario_creacion`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
INSERT INTO `proyecto` VALUES (1,'sdfsdf','sdfsdfsdf',1,'2017-06-29 00:00:00',1),(4,'sdfsdfsss','sdfsdfsdf',1,'2017-06-29 00:00:00',1);
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reportero`
--

DROP TABLE IF EXISTS `reportero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportero` (
  `id_reportero` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_reportero`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reportero`
--

LOCK TABLES `reportero` WRITE;
/*!40000 ALTER TABLE `reportero` DISABLE KEYS */;
INSERT INTO `reportero` VALUES (1,'Pablo Carrillo'),(2,'Gonzalo Muro'),(3,'Noticieros Televisa'),(4,'Indefinido');
/*!40000 ALTER TABLE `reportero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tema`
--

DROP TABLE IF EXISTS `tema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tema` (
  `id_tema` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tema`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tema`
--

LOCK TABLES `tema` WRITE;
/*!40000 ALTER TABLE `tema` DISABLE KEYS */;
/*!40000 ALTER TABLE `tema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'gmurop@gmail.com','Gonzalo Muro','86da6ea9305030f07d140453f9c9ddbd');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-30  9:54:33
