-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: analisismedios
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administracion_proyecto`
--

DROP TABLE IF EXISTS `administracion_proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administracion_proyecto` (
  `fk_id_proyecto` int(11) NOT NULL,
  `fk_id_usuario` int(11) NOT NULL,
  `perfil` int(11) NOT NULL DEFAULT '1',
  KEY `fk_id_proyecto_administracion_idx` (`fk_id_proyecto`),
  KEY `fk_id_usuario_administracion_idx` (`fk_id_usuario`),
  CONSTRAINT `fk_id_proyecto_administracion` FOREIGN KEY (`fk_id_proyecto`) REFERENCES `proyecto` (`id_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_usuario_administracion` FOREIGN KEY (`fk_id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administracion_proyecto`
--

LOCK TABLES `administracion_proyecto` WRITE;
/*!40000 ALTER TABLE `administracion_proyecto` DISABLE KEYS */;
INSERT INTO `administracion_proyecto` VALUES (4,1,1),(5,1,1),(6,1,1),(7,1,1),(8,1,1),(9,1,1),(10,1,1);
/*!40000 ALTER TABLE `administracion_proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conflicto`
--

DROP TABLE IF EXISTS `conflicto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conflicto` (
  `id_conflicto` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id_conflicto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conflicto`
--

LOCK TABLES `conflicto` WRITE;
/*!40000 ALTER TABLE `conflicto` DISABLE KEYS */;
/*!40000 ALTER TABLE `conflicto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medio`
--

DROP TABLE IF EXISTS `medio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medio` (
  `id_medio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_medio`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medio`
--

LOCK TABLES `medio` WRITE;
/*!40000 ALTER TABLE `medio` DISABLE KEYS */;
INSERT INTO `medio` VALUES (1,'MVC Noticias'),(2,'Televisa News'),(3,'Indefinido'),(4,'Gonzalo Muro'),(5,'sdfsd'),(6,'Nuevo medio'),(7,'sdsdf'),(8,'sdf'),(9,'Al calor político');
/*!40000 ALTER TABLE `medio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medio_reportero`
--

DROP TABLE IF EXISTS `medio_reportero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medio_reportero` (
  `fk_id_medio` int(11) NOT NULL,
  `fk_id_repotero` int(11) NOT NULL,
  KEY `fk_id_medio_reportero_idx` (`fk_id_medio`),
  KEY `fk_id_reportero_medio_idx` (`fk_id_repotero`),
  CONSTRAINT `fk_id_medio_reportero` FOREIGN KEY (`fk_id_medio`) REFERENCES `medio` (`id_medio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_reportero_medio` FOREIGN KEY (`fk_id_repotero`) REFERENCES `reportero` (`id_reportero`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medio_reportero`
--

LOCK TABLES `medio_reportero` WRITE;
/*!40000 ALTER TABLE `medio_reportero` DISABLE KEYS */;
/*!40000 ALTER TABLE `medio_reportero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota`
--

DROP TABLE IF EXISTS `nota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota` (
  `id_nota` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_archivo` varchar(100) NOT NULL,
  `clasificacion` varchar(100) DEFAULT NULL,
  `encabezado` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `url_nota` varchar(255) DEFAULT NULL,
  `tendencia` tinyint(4) DEFAULT NULL,
  `fk_id_medio_emitio` int(11) DEFAULT NULL,
  `fk_id_reportero_firmo` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `fk_id_usuario_alta` int(11) DEFAULT NULL,
  `fk_id_proyecto` int(11) NOT NULL,
  `estatus` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id_nota`),
  KEY `fk_id_medio_emitio_idx` (`fk_id_medio_emitio`),
  KEY `fk_id_usuario_alta_idx` (`fk_id_usuario_alta`),
  KEY `fk_id_proyecto_idx` (`fk_id_proyecto`),
  CONSTRAINT `fk_id_medio_emitio` FOREIGN KEY (`fk_id_medio_emitio`) REFERENCES `medio` (`id_medio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_proyecto` FOREIGN KEY (`fk_id_proyecto`) REFERENCES `proyecto` (`id_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_usuario_alta` FOREIGN KEY (`fk_id_usuario_alta`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota`
--

LOCK TABLES `nota` WRITE;
/*!40000 ALTER TABLE `nota` DISABLE KEYS */;
INSERT INTO `nota` VALUES (56,'a583ed1a50c1377c8e24d41724462139.pdf',NULL,'California’s Far North Deplores ‘Tyranny’ of the Urban Majority','2017-07-07','www2',1,2,2,NULL,NULL,4,1),(57,'bab8c5923e851bea9371d485e919da70.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(58,'7e39e3c8f4a31c514ecfc49fafe1cf4d.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(59,'807ec63dc194ff529e5f1dd3f590877e.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(60,'1d7e10fa612080b8e343d1d684950ae4.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(61,'16a0266144d56ad133d93ebbbc04b721.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(62,'84f96f87072240084a4fb78d2e97886e.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(63,'69bda7d011213eda6c83ce4f91bdf5b2.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(64,'90bc8580cffabc277da26e8b2853cf6e.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(65,'c9e09d1f6e22ef418cc8d3116120ded0.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(66,'8f9a3952d92e3120523bebd8c19ed718.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(67,'a0f7d567b32fbe64d7edcad6e1bf6af5.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(68,'b7d4e27a5bbbfa67d996ba43def6318b.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(69,'b4dddc435b58d30a271d3a9b776455ee.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(70,'34cc90c388aa8bf906965de1ce8a5372.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(71,'9aea041e366f009cbc2e02450a8d6c26.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(72,'11f1a383e88bae227148170b4c026d80.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(73,'cd87c152b6fdc249b16584e8ea803a90.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(74,'2c08d901f9df4dc4b0c8476872fdacfe.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(75,'b0fb614dbaf78325169ffa1450d5ad4a.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(76,'69755bc796e20efb9df9e0d1030559b5.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(77,'1ffffe17a6c2a8027c83e6e59ca53933.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(78,'5ee15f99de865b5842ee92688715fc35.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(79,'18381ef715171e00e50607ee2c569a7a.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(80,'ec893fa81dca38047110ae8a0edfaa7d.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(81,'7408b08072cd9ae30c591c55e16cf574.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(82,'367beeea88bf9171058d70e4fe0a71d8.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(83,'6914792913de78c13d837f5d87242ff9.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(84,'7fb8d9ebe68ec1c0a8f8e2a0aa3e92c7.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(85,'0aa9e777ef71a82e145d33193df67974.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(86,'64c0d4842ddfd0a921cf1452f31894b9.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(87,'c922fa5126ccf855842491e70e78544d.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(88,'dd91fa40f635a646f470731b5d5ffa25.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(89,'41a636ce83a676635bd8498e0eb804e1.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(90,'dcebfc5ac0ff712492aaa5b1949ee3bf.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(91,'e409d198da528cf3f89869d5cc1681f5.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(92,'349c972746ec066506a3c9c2debfd5be.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(93,'201eee6b1593c843450020163ca6bbac.pdf',NULL,'sdfsdf','2017-07-12',NULL,0,5,5,NULL,NULL,7,-1),(94,'a909a04aab68ec78d93b7db4101651b9.pdf',NULL,'California’s Far North Deplores ‘Tyranny’ of the Urban Majority','2017-07-05','werwerw',-1,9,2,NULL,NULL,7,1),(95,'00e5027428c93f56c984f64aaf645f84.pdf',NULL,'Encabezado','2017-07-13','asdfasd',0,4,4,NULL,NULL,7,1),(96,'9788c3e1f196ffd7e592c888df4efa31.pdf',NULL,'adsd','2017-07-14','asd',1,6,9,NULL,NULL,7,1),(97,'acedbfe29d62a2044576f329b06e8f88.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,0),(98,'c1d4236404e8bc26456bab1c88602a98.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(99,'02d99f9f8a93fc99de2c8cce9cadd4c3.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(100,'8f2ddbf6b809ba509fa2aab98ee37f65.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0),(101,'de0609771aac4c732cf2e4b03a6bc7f4.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,0),(102,'1a86e82bc4cc9a8c196ec9fccdd29486.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,0),(103,'d325d40238aa569d96311143b170cc7b.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,0),(104,'24b465acdd16285d05603e6cbbfb7553.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,0),(105,'4498bee7b6b8554793d2ec8427516fda.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,0),(106,'0484e25306fe0d2c3a1119a66186df55.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,0),(107,'818b171cfdd0e445cb993f7d4904580a.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,0),(108,'6e594cd5d7e56a4b1473cd8581404e1a.pdf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,0);
/*!40000 ALTER TABLE `nota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota_conflicto`
--

DROP TABLE IF EXISTS `nota_conflicto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_conflicto` (
  `fk_id_nota` int(11) NOT NULL,
  `fk_id_conflicto` int(11) NOT NULL,
  KEY `fk_id_nota_conflicto_idx` (`fk_id_nota`),
  KEY `fk_id_conflicto_nota_idx` (`fk_id_conflicto`),
  CONSTRAINT `fk_id_conflicto_nota` FOREIGN KEY (`fk_id_conflicto`) REFERENCES `conflicto` (`id_conflicto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_nota_conflicto` FOREIGN KEY (`fk_id_nota`) REFERENCES `nota` (`id_nota`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota_conflicto`
--

LOCK TABLES `nota_conflicto` WRITE;
/*!40000 ALTER TABLE `nota_conflicto` DISABLE KEYS */;
/*!40000 ALTER TABLE `nota_conflicto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota_personaje`
--

DROP TABLE IF EXISTS `nota_personaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_personaje` (
  `fk_id_nota` int(11) NOT NULL,
  `fk_id_personaje` int(11) NOT NULL,
  `fk_id_organizacion` int(11) NOT NULL,
  `tendencia` tinyint(4) NOT NULL,
  KEY `fk_id_nota_personaje_idx` (`fk_id_nota`),
  KEY `fk_id_personaje_nota_idx` (`fk_id_personaje`),
  KEY `fk_id_organizacion_nota_idx` (`fk_id_organizacion`),
  CONSTRAINT `fk_id_nota_personaje` FOREIGN KEY (`fk_id_nota`) REFERENCES `nota` (`id_nota`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_organizacion_nota` FOREIGN KEY (`fk_id_organizacion`) REFERENCES `organizacion` (`id_organizacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_personaje_nota` FOREIGN KEY (`fk_id_personaje`) REFERENCES `personaje` (`id_personaje`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota_personaje`
--

LOCK TABLES `nota_personaje` WRITE;
/*!40000 ALTER TABLE `nota_personaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `nota_personaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nota_tema`
--

DROP TABLE IF EXISTS `nota_tema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nota_tema` (
  `fk_id_nota` int(11) NOT NULL AUTO_INCREMENT,
  `fk_id_tema` int(11) DEFAULT NULL,
  `extracto` text,
  PRIMARY KEY (`fk_id_nota`),
  KEY `fk_id_tema_idx` (`fk_id_tema`),
  CONSTRAINT `fk_id_nota` FOREIGN KEY (`fk_id_nota`) REFERENCES `nota` (`id_nota`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_tema` FOREIGN KEY (`fk_id_tema`) REFERENCES `tema` (`id_tema`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nota_tema`
--

LOCK TABLES `nota_tema` WRITE;
/*!40000 ALTER TABLE `nota_tema` DISABLE KEYS */;
INSERT INTO `nota_tema` VALUES (56,2,'r heads mounted on the walls of Eric Johnson’s church\noffice are testament to his passion for hunting, a lifestyle enjoyed by many in the\nnorthernmost reaches of California but one that Mr. Johnson says surprises people\nhe meets on his travels around Americ'),(96,3,'ads');
/*!40000 ALTER TABLE `nota_tema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizacion`
--

DROP TABLE IF EXISTS `organizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizacion` (
  `id_organizacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_organizacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizacion`
--

LOCK TABLES `organizacion` WRITE;
/*!40000 ALTER TABLE `organizacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizacion_personaje`
--

DROP TABLE IF EXISTS `organizacion_personaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizacion_personaje` (
  `fk_id_organizacion` int(11) NOT NULL,
  `fk_id_personaje` int(11) NOT NULL,
  KEY `fk_ir_organizacion_personaje_idx` (`fk_id_organizacion`),
  KEY `fk_id_personaje_organizacion_idx` (`fk_id_personaje`),
  CONSTRAINT `fk_id_organizacion_personaje` FOREIGN KEY (`fk_id_organizacion`) REFERENCES `organizacion` (`id_organizacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_personaje_organizacion` FOREIGN KEY (`fk_id_personaje`) REFERENCES `personaje` (`id_personaje`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizacion_personaje`
--

LOCK TABLES `organizacion_personaje` WRITE;
/*!40000 ALTER TABLE `organizacion_personaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizacion_personaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personaje`
--

DROP TABLE IF EXISTS `personaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personaje` (
  `id_personaje` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_personaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personaje`
--

LOCK TABLES `personaje` WRITE;
/*!40000 ALTER TABLE `personaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `personaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `id_proyecto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_corto` varchar(45) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `fk_id_usuario_creacion` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `estatus` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id_proyecto`),
  UNIQUE KEY `nombre_corto_UNIQUE` (`nombre_corto`),
  KEY `fk_id_usuario_creacion_proyecto_idx` (`fk_id_usuario_creacion`),
  CONSTRAINT `fk_id_usuario_creacion_proyecto` FOREIGN KEY (`fk_id_usuario_creacion`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
INSERT INTO `proyecto` VALUES (1,'sdfsdf','sdfsdfsdf',1,'2017-06-29 00:00:00',1),(4,'sdfsdfsss','sdfsdfsdf',1,'2017-06-29 00:00:00',1),(5,'nuevo','asdfasda',1,'2017-07-03 00:00:00',1),(6,'ssssss','ssssss',1,'2017-07-03 00:00:00',1),(7,'otromas','asasd',1,'2017-07-03 00:00:00',1),(8,'apiver','asds',1,'2017-07-03 00:00:00',1),(9,'dsdsd','sd',1,'2017-07-04 00:00:00',1),(10,'lilia','asdasd',1,'2017-07-07 00:00:00',1);
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reportero`
--

DROP TABLE IF EXISTS `reportero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportero` (
  `id_reportero` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_reportero`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reportero`
--

LOCK TABLES `reportero` WRITE;
/*!40000 ALTER TABLE `reportero` DISABLE KEYS */;
INSERT INTO `reportero` VALUES (1,'Pablo Carrillo'),(2,'Gonzalo Muro'),(3,'Noticieros Televisa'),(4,'Indefinido'),(5,'sdfs'),(6,'Nuevo reportero'),(7,'sdfdf'),(8,'sdfsdf'),(9,'Lilia');
/*!40000 ALTER TABLE `reportero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tema`
--

DROP TABLE IF EXISTS `tema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tema` (
  `id_tema` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  `color` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_tema`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tema`
--

LOCK TABLES `tema` WRITE;
/*!40000 ALTER TABLE `tema` DISABLE KEYS */;
INSERT INTO `tema` VALUES (1,'Economía','orange'),(2,'Social','olive'),(3,'Ecologías y Medio Ambiente','teal'),(4,'Cultural','pink'),(5,'Otro','grey');
/*!40000 ALTER TABLE `tema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'gmurop@gmail.com','Gonzalo Muro','86da6ea9305030f07d140453f9c9ddbd');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-11 14:37:08
