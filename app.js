import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import Cookies from 'universal-cookie';
import * as UsuarioActionCreators from './src/actions/usuario';

import analizadorReducer from './src/reducers/analizador';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

const cookie = new Cookies();

/**
  Único estilo
*/
import './styles/sass/main.scss';

/**
  Componentes propios
*/
import Analizador from './src/containers/Analizador';
import Login from './src/containers/Login';

/**
  Store
*/
const store = createStore(
  analizadorReducer,
  applyMiddleware(thunkMiddleware)
);

const PrivateRoute = ({component: Component, ...rest}) => {

  const state = store.getState();

  const { usuario } = state;

  var token = cookie.get('token');

  if (!usuario.loggedIn && token !== 'undefined') {
    store.dispatch(UsuarioActionCreators.validar(token));
  }

  return (
    <Route {...rest} render={props => (
        usuario.loggedIn ? (
        <Component {...props} />
      ) : (
        <Redirect to={{
          pathname: '/login',
          state: {from: props.location}
        }} />
      )
    )} />
  )
};

ReactDOM.render((
  <Provider store={store}>
    <Router>
      <div>
          <PrivateRoute path="/" component={Analizador} />
          <Route path="/login" component={Login} />
      </div>
    </Router>
  </Provider>
), document.getElementById('app'));
