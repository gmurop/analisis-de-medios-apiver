import { Component } from 'react';
import { Card } from 'semantic-ui-react';

const Proyecto = ({nombreCorto, fecha, descripcion}) => (

  let href = `/proyectos/$(nombreCorto)`;

  <Card
    href={href}
    header={nombreCorto}
    meta={fecha}
    description={descripcion}/>
)

export default Proyecto;
