import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Form, Container, Header, Button } from 'semantic-ui-react';
import { RenderTextField } from './Form/RenderSemanticFields';

const NuevoProyecto = ({handleSubmit, guardarProyecto}) => {

  return (
    <Container style={{'margin-top': '20px'}}>
      <Header as='h1' dividing>Nuevo Proyecto</Header>

      <Form>
        <Field
          name='nombreCorto'
          component={RenderTextField}
          placeholder='Ingrese un nombre corto'
          label='Nombre corto:'/>

        <Field
          name='descripcion'
          component={RenderTextField}
          placeholder='Ingrese una descripción sobre el proyecto'
          label='Descripción:'/>

        <Button onClick={handleSubmit(data => guardarProyecto(data))}>Guardar</Button>


      </Form>
    </Container>
  )

}

NuevoProyecto.propTypes = {
  guardarProyecto: PropTypes.func.isRequired
}

export default reduxForm({
  form: 'NuevoProyecto'
})(NuevoProyecto);
