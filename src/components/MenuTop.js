import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

import {Menu, Icon, Dropdown} from 'semantic-ui-react';

const MenuTop = ({proyectos, usuario, match}) => (
    <div>
          <Menu attached="top" color="pink">

              <Dropdown item text="Proyectos">
                  <Dropdown.Menu>
                      {proyectos.proyectos.map(proyecto => (
                        <NavLink key={proyecto.id_proyecto} to={'/proyectos/' + proyecto.nombre_corto}>
                          <Dropdown.Item>{proyecto.nombre_corto}</Dropdown.Item>
                        </NavLink>
                      ))}

                      <NavLink to='/proyectos/nuevo'>
                        <Dropdown.Item><Icon name="add"/>Nuevo</Dropdown.Item>
                      </NavLink>

                  </Dropdown.Menu>
              </Dropdown>

                  <NavLink to="/reportes">
                    <Menu.Item link>
                      Reportes
                    </Menu.Item>
                  </NavLink>

              <Menu secondary>
                  <Menu.Item name="notas">
                      <Icon name="user"/>
                      {usuario.nombre}
                  </Menu.Item>
              </Menu>

              <Menu.Menu position='right'>
                  <div className='ui right aligned category search item'>
                      <div className='ui transparent icon input'>
                          <input className='prompt' type='text' placeholder='Buscar notas...' />
                          <i className='search link icon' />
                      </div>
                      <div className='results'></div>
                  </div>
              </Menu.Menu>
          </Menu>
          </div>
          )

MenuTop.propTypes = {
  usuario: PropTypes.object.isRequired,
}

MenuTop.defaultProps = {
}

export default MenuTop;
