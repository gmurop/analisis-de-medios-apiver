import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

import {Menu, Label, Button, Icon} from 'semantic-ui-react';

class MenuLeft extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    const {dropzoneRef, proyectos, notas} = this.props;

    return (
            <Menu vertical inverted fixed='left'>
                <Menu.Item className="centered-text" header content='Análisis de Medios' />

                {
                  proyectos.proyectoSeleccionado !== null ?
                  <div>
                    <Menu.Item className="centered-text" header content={proyectos.proyectoSeleccionado.nombreCorto} />
                    <Menu.Item>
                        <Button animated="vertical" inverted color="pink" size="big" fluid onClick={() => dropzoneRef.open()}>
                            <Button.Content visible><Icon name="upload"/></Button.Content>
                            <Button.Content hidden>Subir Notas</Button.Content>
                        </Button>
                    </Menu.Item>

                      <NavLink to="/proyectos/apiver">
                        <Menu.Item name="inbox" link>
                        Notas
                          <Label color='pink'>{notas.notas.filter(nota => nota.estatus === '0').length}</Label>
                        </Menu.Item>
                      </NavLink>

                    <Menu.Item name="inbox">
                      Filtros...
                    </Menu.Item>
                  </div> :
                  ''
                }

            </Menu>
            );
  }
}

export default MenuLeft;
