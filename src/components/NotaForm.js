import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form as ReduxForm, Field, reduxForm } from 'redux-form';
import { Form, Button, Message, Confirm } from 'semantic-ui-react';
import { RenderTextField, RenderDateField, RenderDropdownField, RenderTendenciaField } from './Form/RenderSemanticFields';
import moment from 'moment';

class NotaForm extends Component {

    componentWillReceiveProps(nextProps) {

      if (nextProps.notaDetalles === this.props.notaDetalles)
        return;

      const { id_nota, encabezado, url_nota, fecha, medio, reportero, tendencia } = nextProps.notaDetalles;

      //Inicializar form
      this.props.initialize({
        id_nota,
        encabezado,
        url_nota,
        fecha,
        medio,
        reportero,
        tendencia: Number(tendencia)
      })
    }

    componentWillUnmount() {
      console.log("unmounted");
    }

    openConfirmEliminar(e) {
      e.preventDefault();
      this.setState({confirmEliminarOpen: true});
    }

    handleCancelEliminar() {
      this.setState({confirmEliminarOpen: false});
    }

    handleConfirmEliminar() {
      this.setState({confirmEliminarOpen: false});
      this.eliminarNota();
    }

    render() {

      const { notaDetalles, handleSubmit, eliminarNota } = this.props;

        return (
                <Form as={ReduxForm} onSubmit={handleSubmit}>

                    <Field
                      name="encabezado"
                      component={RenderTextField}
                      placeholder="Ingrese el encabezado"
                      label="Encabezado:"/>

                    <Field
                      name="url_nota"
                      component={RenderTextField}
                      placeholder="Ingrese la URL"
                      label="URL:"/>

                    <Field
                      name="fecha"
                      component={RenderDateField}
                      label="Fecha:"/>

                    <Field
                      name="medio"
                      component={RenderDropdownField}
                      label="Medio:"
                      additionLabel="Agregar medio: "
                      allowAdditions
                      placeholder="Ingrese el medio"
                      options={this.props.medios}
                      agregarItem={this.props.agregarMedio}
                      search
                      selection />

                    <Field
                      name="reportero"
                      component={RenderDropdownField}
                      label="Reportero:"
                      additionLabel="Agregar reportero: "
                      allowAdditions
                      placeholder="Ingrese el reportero"
                      options={this.props.reporteros}
                      agregarItem={this.props.agregarReportero}
                      search
                      selection />

                    <Field
                      name="tendencia"
                      label="Tendencia:"
                      component={RenderTendenciaField}/>

                    <Message
                      success
                      hidden={!this.props.notaActualizada}
                      header='Nota actualizada'
                      content='La nota fue actualizada exitosamente.'/>

                    <Confirm
                      cancelButton='Cancelar'
                      confirmButton='Eliminar'
                      content='¿Realmente desea eliminar esta nota?'
                      header='Eliminar nota'
                      open={this.props.confirmEliminarOpen}
                      onCancel={this.props.cancelarEliminarNota}
                      onConfirm={this.props.confirmarEliminarNota}/>

                    <Button floated="right">Guardar</Button>
                    <Button
                      floated="right"
                      onClick={e => {e.preventDefault(); eliminarNota(notaDetalles.id_nota)}}>Eliminar</Button>
                </Form>
                );
    }
}

NotaForm.propTypes = {
  medios: PropTypes.array.isRequired,
  reporteros: PropTypes.array.isRequired,
  agregarMedio: PropTypes.func.isRequired,
  agregarReportero: PropTypes.func.isRequired,
  eliminarNota: PropTypes.func.isRequired,
}

export default reduxForm({
  form: 'notaForm'
})(NotaForm);
