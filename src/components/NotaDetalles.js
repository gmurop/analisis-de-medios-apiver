import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Icon, Grid, Button, Header, Accordion, Segment} from 'semantic-ui-react';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';

import NotaForm from './NotaForm';
import TemaForm from './TemaForm';
import PersonajeForm from './PersonajeForm';
import ConflictoForm from './ConflictoForm';


class NotaDetalles extends Component {

    render() {

      const nombreArchivoNota = '/backend/notas/' + this.props.notaDetalles.nombre_archivo;

        return (
                <div className="nota-detalles">
                    <a
                      onClick={() => this.props.toggleSidebar(false)}
                      className="sidebar-close">
                            <Icon name="close" />
                    </a>

                    <Grid columns={2} divided className="archivo-container">
                            <Grid.Column width="10" className="archivo-container">
                                {
                                  (this.props.notaDetalles.estatus === '1') ?
                                  <Header as='h3' textAlign='center'>
                                    {this.props.notaDetalles.encabezado}
                                    <Header.Subheader>
                                      <a href={this.props.notaDetalles.url_nota} target='_blank'>{this.props.notaDetalles.url_nota}</a>
                                    </Header.Subheader>
                                  </Header> :
                                    ''
                                }
                                <object
                                  data={nombreArchivoNota}
                                  type="image/jpeg"
                                  width="100%"
                                  className="archivo-container">
                                    alt : <a href={nombreArchivoNota}>{nombreArchivoNota}</a>
                                </object>
                            </Grid.Column>

                            <Grid.Column width="6">
                                <Tabs forceRenderTabPanel>
                                    <TabList>
                                        <Tab>Nota</Tab>
                                        <Tab>Temas</Tab>
                                        <Tab>Personajes</Tab>
                                        <Tab>Conflictos</Tab>
                                    </TabList>

                                    <TabPanel>
                                        <NotaForm
                                            toggleSidebar={this.props.toggleSidebar}
                                            notaDetalles={this.props.notaDetalles}
                                            medios={this.props.medios}
                                            reporteros={this.props.reporteros}
                                            agregarMedio={this.props.agregarMedio}
                                            agregarReportero={this.props.agregarReportero}
                                            eliminarNota={this.props.eliminarNota}
                                            onSubmit={this.props.actualizarNota}/>
                                    </TabPanel>
                                    <TabPanel>
                                      <TemaForm
                                          notaDetalles={this.props.notaDetalles}
                                          eliminartema={this.props.eliminarTema}
                                          temas={this.props.temas}
                                          onSubmit={this.props.agregarTema}/>
                                    </TabPanel>
                                    <TabPanel>
                                        <PersonajeForm
                                            notaDetalles={this.props.notaDetalles}
                                            agregarPersonaje={this.props.agregarPersonaje}
                                            eliminarPersonaje={this.props.eliminarPersonaje}
                                            personajes={this.props.personajes}/>
                                    </TabPanel>
                                    <TabPanel>
                                        <ConflictoForm
                                            notaDetalles={this.props.notaDetalles}
                                            agregarConflicto={this.props.agregarConflicto}
                                            eliminarConflicto={this.props.eliminarConflicto}
                                            conflictos={this.props.conflictos}/>
                                    </TabPanel>
                                </Tabs>
                            </Grid.Column>
                    </Grid>
                </div>

                );
    }
}

NotaDetalles.propTypes = {
  nota: PropTypes.shape({
    nombre_archivo: PropTypes.string
  }),
  medios: PropTypes.array.isRequired,
  reporteros: PropTypes.array.isRequired,
  agregarMedio: PropTypes.func.isRequired,
  agregarReportero: PropTypes.func.isRequired,
  actualizarNota: PropTypes.func.isRequired
}

NotaDetalles.defaultProps = {
  nota: {
    nombre_archivo: ''
  },
  temas: []
}

export default NotaDetalles;
