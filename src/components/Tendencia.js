import React from 'react';
import {Button} from 'semantic-ui-react';

/**
  Component: Tendencia
  Description:
*/

const Tendencia = (props) => {
  
  return (
          <Button.Group fluid>
              <Button
                  content="Positivo"
                  className={props.value === 1 ? 'green' : ''}
                  icon="thumbs up"
                  labelPosition="left"
                  onClick={(e) => { e.preventDefault(); props.onChange(1)} }/>
              <Button.Or text="O"/>
              <Button
                  content="Neutro"
                  className={props.value === 0 ? 'blue' : ''}
                  onClick={(e) => { e.preventDefault(); props.onChange(0)} }/>
              <Button.Or text="O"/>
              <Button
                  content="Negativo"
                  className={props.value === -1 ? 'red' : ''}
                  icon="thumbs down"
                  labelPosition="right"
                  onClick={(e) => { e.preventDefault(); props.onChange(-1)} }/>
          </Button.Group>
        );
};

export default Tendencia;
