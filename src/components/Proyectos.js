import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Switch, Route, Redirect} from 'react-router-dom';
import { Container, Card } from 'semantic-ui-react';

import Notas from './Notas';
import Reportes from './Reportes';
import NuevoProyecto from './NuevoProyecto';

class Proyectos extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    //this.props.cargarProyectos();
    this.props.cargarReporteros();
    this.props.cargarMedios();
    this.props.cargarTemas();
  }

  render () {

    const {
      cargarNotas,
      verDetallesNota,
      sidebarVisible,
      toggleSidebar,
      onDrop,
      notas,
      notaDetalles,
      medios,
      reporteros,
      agregarMedio,
      agregarReportero,
      guardarProyecto,
      redirectedToUltimoProyecto,
      proyectos,
      seleccionarProyecto,
      setDropzoneRef,
      actualizarNota,
      eliminarNota,
      temas,
      agregarTema,
      eliminarTema,
      personajes,
      agregarPersonaje,
      eliminarPersonaje
    } = this.props;

    if (proyectos.redirectToUltimoProyecto) {

      const ultimoProyecto = proyectos.proyectos[proyectos.proyectos.length - 1];
      return (
        <Redirect push to={{pathname: '/proyectos/' + ultimoProyecto.nombre_corto}} />
      )
    }

    return (
      <div>
        <Switch>
          <Route path='/proyectos/nuevo' render={props => (
            <NuevoProyecto
              guardarProyecto={guardarProyecto}/>
          )} />
          <Route path='/proyectos/:proyecto' render={props => (
            <Notas
              {...props}
              cargarNotas={cargarNotas}
              verDetallesNota={verDetallesNota}
              sidebarVisible={sidebarVisible}
              toggleSidebar={toggleSidebar}
              onDrop={onDrop}
              notas={notas}
              notaDetalles={notaDetalles}
              medios={medios}
              reporteros={reporteros}
              agregarMedio={agregarMedio}
              agregarReportero={agregarReportero}
              proyectos={proyectos}
              seleccionarProyecto={seleccionarProyecto}
              setDropzoneRef={setDropzoneRef}
              actualizarNota={actualizarNota}
              eliminarNota={eliminarNota}
              temas={temas}
              agregarTema={agregarTema}
              eliminarTema={eliminarTema}
              personajes={personajes}
              agregarPersonaje={agregarPersonaje}
              eliminarPersonaje={eliminarPersonaje}/>
          )}/>
          <Route path="/proyectos/:proyecto/reportes" component={Reportes} />
          <Route render={(props) => (
            <Card.Group style={{margin: 0}}>

                <Card
                  href='/proyectos/apiver'
                  header='Nombre corto'
                  meta='01/01/2017'
                  description='Esta es una descripción de prueba del proyecto'/>

            </Card.Group>
          )
          } />
        </Switch>
      </div>
    );

  }

  componentDidUpdate(prevProps) {
    if (prevProps.proyectos.redirectToUltimoProyecto) {
      this.props.redirectedToUltimoProyecto();
    }
    if(this.props.proyectos.redirectToUltimoProyecto) {
      //
    }
  }
}

Proyectos.PropTypes = {
  notas: PropTypes.array.isRequired,
  cargarNotas: PropTypes.func.isRequired,
  sidebarVisible: PropTypes.bool.isRequired,
  verDetallesNota: PropTypes.func.isRequired,
  toggleSidebar: PropTypes.func.isRequired,
  onDrop: PropTypes.func.isRequired,
  notaDetalles: PropTypes.object.isRequired,
  medios: PropTypes.array.isRequired,
  reporteros: PropTypes.array.isRequired,
  agregarMedio: PropTypes.func.isRequired,
  agregarReportero: PropTypes.func.isRequired,
  cargarProyectos: PropTypes.func.cargarProyectos,
  guardarProyecto: PropTypes.func.isRequired
}

export default Proyectos;
