import React from 'react';
import { Form, TextArea } from 'semantic-ui-react';
import { DatePicker, DatePickerInput } from 'rc-datepicker';
import Tendencia from '../Tendencia';

const DateField = ({input, ...custom}) => (
  <DatePickerInput
    {...input}
    {...custom}/>
);

export const RenderTextField = ({input, label, placeholder, name, ...custom}) => (
  <Form.Input
      label={label}
      type="text"
      required
      placeholder={placeholder}
      name={name}
      {...input}/>
);

export const RenderDateField = ({input, label, name, ...custom}) => (
  <Form.Field
    control={DateField}
    label={label}
    name={name}
    {...input}/>
);

export const RenderDropdownField = ({input, label, name, additionLabel, allowAdditions, placeholder, options, agregarItem, search, selection, ...custom}) => (
  <Form.Dropdown
      name={name}
      label={label}
      value={input.value}
      additionLabel={additionLabel}
      allowAdditions={allowAdditions}
      placeholder={placeholder}
      options={options}
      onChange={(param, data) => input.onChange(data.value)}
      onAddItem={(e, {value}) => agregarItem(value)}
      search={search}
      selection={selection}
      {...input}
      required/>
);

export const RenderTendenciaField = ({input, label, ...custom}) => (
  <Form.Field
      control={Tendencia}
      label={label}
      {...input}
      {...custom}/>
)

export const RenderTextAreaField = ({input, label, placeholder, name, ...custom}) => (
  <Form.Field
    name={name}
    control={TextArea}
    label={label}
    placeholder={placeholder}
    {...input}
    required/>
)
