import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import {Table, Icon, Sidebar, Segment} from 'semantic-ui-react';
import axios from 'axios';
import Nota from './Nota';
import NotaDetalles from './NotaDetalles';

class Notas extends Component {

    constructor(props) {
      super(props);

      this.dropzoneRef;
    }

    componentWillReceiveProps(nextProps) {
      let proyectoActual = this.props.match.params.proyecto,
          proyectoSiguiente = nextProps.match.params.proyecto;
      if (!proyectoActual || proyectoActual !== proyectoSiguiente) {
        let proyecto = this.getProyectoSeleccioando(nextProps.match.params.proyecto, nextProps.proyectos.proyectos);
        this.props.seleccionarProyecto(proyecto);
        this.props.cargarNotas(proyecto.id_proyecto);
      }
    }

    componentDidMount() {
      this.props.setDropzoneRef(this.dropzoneRef);

      if (!this.props.proyectos.proyectoSeleccionado) {
        let proyecto = this.getProyectoSeleccioando(this.props.match.params.proyecto, this.props.proyectos.proyectos);
        this.props.seleccionarProyecto(proyecto);
        this.props.cargarNotas(proyecto.id_proyecto);
      }


    }

    render() {

        let dropzoneRef;

        let dropzoneStyle = {
        width: '100%',
                minHeight: '600px',
                border: '2px solid #fff'
        },
        dropzoneActiveStyle = {
        border: '2px dashed #c9c9c9'
        };

        let { proyectoSeleccionado } = this.props.proyectos;

        return (
                <Sidebar.Pushable as={Segment}>
                    <Sidebar
                        as={Segment}
                        animation="overlay"
                        direction="right"
                        visible={this.props.sidebarVisible}
                        className="sidebar-nota-detalles">

                        <NotaDetalles
                          toggleSidebar={this.props.toggleSidebar}
                          notaDetalles={this.props.notaDetalles}
                          medios={this.props.medios}
                          reporteros={this.props.reporteros}
                          agregarMedio={this.props.agregarMedio}
                          agregarReportero={this.props.agregarReportero}
                          actualizarNota={this.props.actualizarNota}
                          eliminarNota={this.props.eliminarNota}
                          temas={this.props.temas}
                          agregarTema={this.props.agregarTema}
                          eliminarTema={this.props.eliminarTema}
                          personajes={this.props.personajes}
                          agregarPersonaje={this.props.agregarPersonaje}
                          eliminarPersonaje={this.props.eliminarPersonaje}/>

                    </Sidebar>
                    <Sidebar.Pusher>
                        <Dropzone ref={node => {this.dropzoneRef = node;} }
                                  style={dropzoneStyle}
                                  activeStyle={dropzoneActiveStyle}
                                  onDrop={files => this.props.onDrop(proyectoSeleccionado.id_proyecto, files)}
                                  disableClick>
                            <Table celled striped>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell collapsing>Clasificación</Table.HeaderCell>
                                        <Table.HeaderCell>Encabezado</Table.HeaderCell>
                                        <Table.HeaderCell>Reportero</Table.HeaderCell>
                                        <Table.HeaderCell>Medio</Table.HeaderCell>
                                        <Table.HeaderCell>Fecha</Table.HeaderCell>
                                        <Table.HeaderCell>Temas</Table.HeaderCell>
                                        <Table.HeaderCell collapsing>
                                            <Icon name="legal" />
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>

                                    {
                                    this.props.notas.map((nota, i) =>
                                        <Nota
                                        key={nota.id_nota}
                                        id={nota.id_nota}
                                        clasificacion={nota.clasificacion}
                                        encabezado={nota.encabezado}
                                        nombreArchivo={nota.nombre_archivo}
                                        reportero={nota.reportero}
                                        medio={nota.medio}
                                        fecha={nota.fecha}
                                        temas={nota.temas}
                                        tendencia={nota.tendencia}
                                        onClickNota={() => this.props.verDetallesNota(i)}
                                        estatus={nota.estatus}
                                        />)
                                    }

                                </Table.Body>
                            </Table>
                        </Dropzone>
                    </Sidebar.Pusher>
                </Sidebar.Pushable>
                );
    }

    getProyectoSeleccioando(nombreCortoProyecto, proyectos) {
      return proyectos.find(proyecto => proyecto.nombre_corto === nombreCortoProyecto) || null;
    }
}

Notas.propTypes = {
  notas: PropTypes.array.isRequired,
  sidebarVisible: PropTypes.bool.isRequired,
  cargarNotas: PropTypes.func.isRequired,
  toggleSidebar: PropTypes.func.isRequired,
  onDrop: PropTypes.func.isRequired,
  notaDetalles: PropTypes.object.isRequired,
  medios: PropTypes.array.isRequired,
  reporteros: PropTypes.array.isRequired,
  agregarMedio: PropTypes.func.isRequired,
  agregarReportero: PropTypes.func.isRequired,
  actualizarNota: PropTypes.func.isRequired,
  temas: PropTypes.array.isRequired
}

export default Notas;
