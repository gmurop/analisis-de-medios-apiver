import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Table, Icon, Label} from 'semantic-ui-react';

function IconTendencia (props) {

    switch(Number(props.tendencia)) {
        case -1:
            return <Icon name="long arrow down" color="red"/>;
            break;
        case 0:
            return <Icon name="minus" color="blue"/>;
        case 1:
            return <Icon name="long arrow up" color="green"/>;
        default:
            return null;
    }
}

class Nota extends Component {

    render() {
        return (
                <Table.Row
                className={'nota ' + (this.props.estatus === '0' ? 'pendiente' : '')}
                onClick={this.props.onClickNota}>

                    <Table.Cell>{this.props.id}</Table.Cell>
                    <Table.Cell>{this.props.encabezado || this.props.nombreArchivo}</Table.Cell>
                    <Table.Cell>{this.props.reportero}</Table.Cell>
                    <Table.Cell>{this.props.medio}</Table.Cell>
                    <Table.Cell>{this.props.fecha}</Table.Cell>
                    <Table.Cell>
                        {this.props.temas.map((tema, i) => <Label key={i} tag color="green"><Icon name="tag"/></Label>)}
                    </Table.Cell>
                    <Table.Cell>{(this.props.estatus === '1') ? <IconTendencia tendencia={this.props.tendencia}/> : ''}</Table.Cell>

                </Table.Row>
               );
    }

}

Nota.PropTypes = {
  id: PropTypes.number.isRequired,
  onClickNota: PropTypes.func.isRequired
}

export default Nota;
