import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Form as ReduxForm, Field, reduxForm } from 'redux-form';
import { Form, Button, Dropdown, Header, Icon, Segment, TextArea } from 'semantic-ui-react';
import { RenderDropdownField, RenderTextAreaField } from './Form/RenderSemanticFields';

class TemaForm extends Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.notaDetalles === this.props.notaDetalles)
      return;

    const { id_nota, temas } = nextProps.notaDetalles;
    this.props.initialize({
      id_nota,
      temas
    });

  }

  render() {

    const { notaDetalles, handleSubmit, eliminarTema, temas } = this.props;

    return (
            <Form as={ReduxForm} onSubmit={handleSubmit}>

            {
              this.props.temas.map(tema => (
                <div key={tema.key}>
                  <Header as='h5' attached='top'><Icon color={tema.color} name='tag'/> {tema.text}</Header>
                  <Segment attached>
                    <Field
                      name={'tema_' + tema.value}
                      component={({input, name, ...custom}) => (
                        <Form.Field
                          name={name}
                          placeholder='Agrega un extracto sobre lo que se dijo de este tema.'
                          control={TextArea}
                          rows='2'
                          {...input}
                        />
                      )}
                    />
                  </Segment>
                </div>
              ))
            }

            <Button floated="right">Guardar</Button>

            </Form>
            );
  }
}

TemaForm.propTypes = {
  temas: PropTypes.array.isRequired,
  notaDetalles: PropTypes.object.isRequired,
}

export default reduxForm({
  form: 'temaForm'
})(TemaForm);
