import React from 'react';

import {Form, Button, TextArea} from 'semantic-ui-react';

const ConflictoForm = () => (
    <Form>
            <Form.Input label="Título:" type="text" placeholder="Ingrese el título" required/>
            <Form.Field label="Conflicto:" control={TextArea} placeholder="¿Qué se dice?" required/>
            <Button floated="right">Guardar</Button>
        </Form>
    )

export default ConflictoForm;
