import React from 'react';
import ReacDOM from 'react-dom';
import {Form, Button, TextArea, Segment, Accordion} from 'semantic-ui-react';

import Tendencia from './Tendencia';

const panels = [{title: 'Título', content: 'Contenido'}];

const PersonajeForm = () => (
    <div>
        <Segment>
            <Form>
                <Form.Input label="Tema:" type="text" required/>
                <Form.Input label="Organización:" type="text" required/>
                <Form.Field control={Tendencia} label="Tendencia" />
                <Button>Guardar</Button>
            </Form>
        </Segment>

        <Segment>
            <Accordion panels={panels} exclusive={false} fluid styled/>
        </Segment>
    </div>
    )

export default PersonajeForm;
