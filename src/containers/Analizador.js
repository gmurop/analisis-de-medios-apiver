import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as notasActionCreators from '../actions/nota';
import * as proyectosActionCreators from '../actions/proyecto';

import {Switch, Route, withRouter} from 'react-router-dom';

import {Grid, Menu, Label} from 'semantic-ui-react';

//Componentes
import Notas from '../components/Notas';
import Proyectos from '../components/Proyectos';
import Reportes from '../components/Reportes';
import MenuLeft from '../components/MenuLeft';
import MenuTop from '../components/MenuTop';

class Analizador extends Component {

  render() {

    const {dispatch, notas, proyectos, usuario, match} = this.props;

    let accionesNotas = bindActionCreators(notasActionCreators, dispatch);
    let accionesProyectos = bindActionCreators(proyectosActionCreators, dispatch);

    return (
            <div>

                <MenuLeft dropzoneRef={notas.dropzoneRef} notas={notas} proyectos={proyectos}/>

                <div style={{marginLeft: '210px'}}>
                    <div>
                        <MenuTop proyectos={proyectos} usuario={usuario} match={match}/>
                    </div>

                    <div>
                      <Switch>
                        <Route path="/proyectos" render={props => (
                          <Proyectos
                            {...props}
                            {...accionesNotas}
                            {...accionesProyectos}
                            proyectos={proyectos}
                            notas={notas.notas}
                            notaDetalles={notas.notaDetalles}
                            sidebarVisible={notas.sidebarVisible}
                            medios={notas.medios}
                            reporteros={notas.reporteros}
                            temas={notas.temas}
                            personajes={notas.personajes}/>
                        )}/>
                        <Route path="/reportes" component={Reportes}/>
                      </Switch>
                    </div>
                </div>

            </div>
            );
  }

}

Analizador.PropTypes = {
  usuario: PropTypes.object.isRequired,
  notas: PropTypes.object.isRequired
}


const mapStateProps = state => (
  {
    usuario: state.usuario,
    notas: state.notas,
    proyectos: state.proyectos,
  }
);

export default withRouter(connect(mapStateProps)(Analizador));
