import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as UsuarioActionCreators from '../actions/usuario';
import { Button, Form, Segment } from 'semantic-ui-react';
import { Redirect, withRouter } from 'react-router-dom';

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    }
  }

  handleChangeEmail(event) {
    this.setState({
      email: event.target.value
    });
  }

  handleChangePassword(event) {
    this.setState({
      password: event.target.value
    })
  }

  render() {

    const { dispatch, usuario } = this.props;
    let accionesUsuario = bindActionCreators(UsuarioActionCreators, dispatch);

    //let { from } = this.props.location.state || {from: {pathname: '/'}};
    let { from } = {from: {pathname: '/'}};

    if (usuario.redirectToReferrer) {
      return (
        <Redirect to={from} />
      )
    }

    return (
      <div className="ui one column stackable center aligned page grid">
         <div className="column twelve wide">
         <Segment inverted padded size='small'>
           <Form onSubmit={(e) => {e.preventDefault(); accionesUsuario.login(this.state.email, this.state.password)}} inverted>
             <Form.Group widths='equal'>
               <Form.Input label='Email' placeholder='Email' value={this.state.email} onChange={this.handleChangeEmail.bind(this)}/>
               <Form.Input type='password' label='Contraseña' placeholder='Contraseña' value={this.state.password} onChange={this.handleChangePassword.bind(this)}/>
             </Form.Group>

             <Button type='submit'>Iniciar sesión</Button>
           </Form>
         </Segment>
         </div>
      </div>
    );
  }
}

const mapStateProps = state => (
  {
    usuario: state.usuario
  }
);

export default withRouter(connect(mapStateProps)(Login));
