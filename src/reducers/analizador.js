import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import notas from './nota';
import proyectos from './proyecto';
import usuario from './usuario';

const analizadorReducer = combineReducers({
  usuario,
  notas,
  proyectos,
  form: formReducer
});

export default analizadorReducer;
