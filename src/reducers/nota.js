import * as NotaActionTypes from '../actiontypes/nota';
import update from 'immutability-helper';

const initialState = {
  notas: [],
  notaDetalles: {},
  cargandoNotas: false,
  sidebarVisible: false,
  medios: [],
  reporteros: [],
  dropzoneRef: null,
  cargandoMedios: false,
  cargandoReporteros: false,
  cargandoTemas: false,
  cargandoPersonajes: false,
  temas: [],
  personajes: [],
  actualizandoNota: false,
  agregandoTema: false
}

export default function notas(state = initialState, action) {

  let indexNota;

  switch(action.type) {

    case NotaActionTypes.CARGANDO_NOTAS:
      return {
        ...state,
        cargandoNotas: true
      }

    case NotaActionTypes.CARGANDO_NOTAS_SUCCESS:
      return {
        ...state,
        notas: action.notas,
        cargandoNotas: false
      };

    case NotaActionTypes.TOGGLE_DETALLES_NOTA:
      return {
        ...state,
        sidebarVisible: action.toggleSidebar
      }

    case NotaActionTypes.VER_DETALLES_NOTA:
      return {
        ...state,
        sidebarVisible: action.toggleSidebar,
        notaDetalles: state.notas.find((nota, i) => i === action.index)
      }

    case NotaActionTypes.ACTUALIZANDO_NOTA:
      return {
        ...state,
        actualizandoNota: true
      }

    case NotaActionTypes.ACTUALIZANDO_NOTA_SUCCESS:
      indexNota = state.notas.findIndex(nota => nota.id_nota = action.notaActualizada.id_nota);
      console.log(indexNota, state.notas, typeof action.notaActualizada);
      return {
        ...state,
        actualizandoNota: false,
        notas: update(state.notas, {[indexNota]: {$merge: action.notaActualizada}})
      }

    case NotaActionTypes.ACTUALIZANDO_NOTA_ERROR:
      return {
        ...state,
        actualizandoNota: false
      }

    case NotaActionTypes.AGREGAR_MEDIO:
      return {
        ...state,
        medios: [...state.medios, {key: state.medios.length, ...action.medio}]
      }

    case NotaActionTypes.AGREGAR_REPORTERO:
      return {
        ...state,
        reporteros: [...state.reporteros, {key: state.reporteros.length, ...action.reportero}]
      }

    case NotaActionTypes.AGREGANDO_NOTA_SUCCESS:
      return {
        ...state,
        notas: [...state.notas, ...action.notasAgregadas]
      }

    case NotaActionTypes.SET_DROPZONE_REF:
      return {
        ...state,
        dropzoneRef: action.dropzoneRef
      }

    case NotaActionTypes.CARGANDO_MEDIOS:
      return {
        ...state,
        cargandoMedios: action.cargandoMedios
      }

    case NotaActionTypes.CARGANDO_MEDIOS_SUCCESS:
      return {
        ...state,
        cargandoMedios: action.cargandoMedios,
        medios: [...state.medios, ...action.medios]
      }

    case NotaActionTypes.CARGANDO_MEDIOS_ERROR:
      return {
        ...state,
        cargandoMedios: action.cargandoMedios,
      }

    case NotaActionTypes.CARGANDO_REPORTEROS:
      return {
        ...state,
        cargandoReporteros: action.cargandoReporteros,
      }

    case NotaActionTypes.CARGANDO_REPORTEROS_SUCCESS:
      return {
        ...state,
        cargandoReporteros: action.cargandoReporteros,
        reporteros: [...state.reporteros, ...action.reporteros]
      }

    case NotaActionTypes.CARGANDO_REPORTEROS_ERROR:
      return {
        ...state,
        cargandoReporteros: action.cargandoReporteros
      }

    case NotaActionTypes.CARGANDO_TEMAS:
      return {
        ...state,
        cargandoTemas: true
      }

    case NotaActionTypes.CARGANDO_TEMAS_SUCCESS:
      return {
        ...state,
        cargandoTemas: false,
        temas: [...state.temas, ...action.temas]
      }

    case NotaActionTypes.CARGANDO_TEMAS_ERROR:
      return {
        ...state,
        cargandoTemas: false,
      }

    case NotaActionTypes.CARGANDO_PERSONAJES:
      return {
        ...state,
        cargandoPersonajes: true
      }

    case NotaActionTypes.CARGANDO_PERSONAJES_SUCCESS:
      return {
        ...state,
        cargandoPersonajes: false,
        personajes: [...state.personajes, ...action.personajes]
      }

    case NotaActionTypes.CARGANDO_PERSONAJES_ERROR:
      return {
        ...state,
        cargandoPersonajes: false,
      }

    case NotaActionTypes.AGREGANDO_TEMA:
      return {
        ...state,
        agregandoTema: true
      }

    case NotaActionTypes.AGREGANDO_TEMA_SUCCESS:
      indexNota = state.notas.findIndex(nota => nota.id_nota = action.tema.idNota);
      return {
        ...state,
        agregandoTema: false,
        tema: update(state.notas, {[indexNota]: {temas: {$push: action.tema}}})
      }

    case NotaActionTypes.AGREGANDO_TEMA_ERROR:
      return {
        ...state,
        agregandoTema: false
      }

    default:
      return state;
  }

}
