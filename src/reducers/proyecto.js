import * as ProyectoActionTypes from '../actiontypes/proyecto';

const initialState = {
  proyectos: [],
  cargandoProyectos: false,
  redirectToUltimoProyecto: false,
  proyectoSeleccionado: null,
}

export default function proyectoReducer(state = initialState, action) {

  switch(action.type) {
    case ProyectoActionTypes.CARGANDO_PROYECTOS:
      return {
        ...state,
        cargandoProyectos: action.cargandoProyectos
      }

    case ProyectoActionTypes.CARGANDO_PROYECTOS_SUCCESS:
      return {
        ...state,
        cargandoProyectos: action.cargandoProyectos,
        proyectos: action.proyectos
      }

    case ProyectoActionTypes.GUARDANDO_PROYECTO:
      return {
        ...state,
        guardandoProyecto: true
      }

    case ProyectoActionTypes.GUARDANDO_PROYECTO_SUCCESS:
      return {
        ...state,
        guardandoProyecto: action.guardandoProyecto,
        proyectoGuardado: action.proyectoGuardado,
        redirectToUltimoProyecto: action.redirectToUltimoProyecto,
        proyectos: [...state.proyectos, action.proyecto]
      }

    case ProyectoActionTypes.REDIRECTED_ULTIMO_PROYECTO:
      let { redirectedToUltimoProyecto } = action;
      return {
        ...state,
        redirectedToUltimoProyecto
      }

    case ProyectoActionTypes.SELECCIONAR_PROYECTO:
      return {
        ...state,
        proyectoSeleccionado: action.proyectoSeleccionado
      }

    default:
    return state;
  }

}
