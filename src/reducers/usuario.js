import * as UsuarioActionTypes from '../actiontypes/usuario';

const initialState = {
  loggedIn: false,
  redirectToReferrer: false,
  iniciandoSesion: false,
  idUsuario: null,
  nombre: '',
  email: '',
}

export default function notas(state = initialState, action) {

  switch(action.type) {

    case UsuarioActionTypes.INICIANDO_SESION:
      return {
        ...state,
        iniciandoSesion: true
      };

    case UsuarioActionTypes.INICIANDO_SESION_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        iniciandoSesion: false,
        redirectToReferrer: true,
        idUsuario: action.idUsuario,
        nombre: action.nombre,
        email: action.email,
      };

    case UsuarioActionTypes.INICIANDO_SESION_ERROR:
      return {
        ...state,
        loggedIn: false,
        iniciandoSesion: false
      };

    case UsuarioActionTypes.VALIDANDO_TOKEN_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        iniciandoSesion: false,
        redirectToReferrer: true,
        idUsuario: action.idUsuario,
        nombre: action.nombre,
        email: action.email
      }

    default:
      return state;
  }

}
