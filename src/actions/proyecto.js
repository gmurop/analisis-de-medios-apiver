import axios from 'axios';
import Cookies from 'universal-cookie';
import * as ProyectoActionTypes from '../actiontypes/proyecto';

const cookie = new Cookies();

axios.defaults.headers.common['Authorization'] = cookie.get('token');

export const agregarProyecto = (proyecto) => {
  return {
    type: ProyectoActionTypes.AGREGAR_PROYECTO,
    ...proyecto
  }
};

export const seleccionarProyecto = proyectoSeleccionado => {
  return {
    type: ProyectoActionTypes.SELECCIONAR_PROYECTO,
    proyectoSeleccionado
  }
}

export const cargandoProyectos = () => {
  return {
    type: ProyectoActionTypes.CARGANDO_PROYECTOS,
    cargandoProyectos: true
  }
}

export const cargandoProyectosSuccess = proyectos => {
  return {
    type: ProyectoActionTypes.CARGANDO_PROYECTOS_SUCCESS,
    cargandoProyectos: false,
    proyectos
  }
}

export const guardandoProyectoSuccess = (proyecto) => {
  return {
    type: ProyectoActionTypes.GUARDANDO_PROYECTO_SUCCESS,
    guardandoProyecto: false,
    proyectoGuardado: true,
    redirectToUltimoProyecto: true,
    proyecto
  }
}

export const redirectedToUltimoProyecto = () => {
  return {
    type: ProyectoActionTypes.REDIRECTED_ULTIMO_PROYECTO,
    redirectToUltimoProyecto: false
  }
}

/**
  Acciones asíncronas
*/

export function cargarProyectos(idUsuario = 1) {
  return function(dispatch) {
    dispatch(cargandoProyectos());

    axios.get(`http://localhost/analisismedios/backend/api/proyecto/${idUsuario}`)
      .then(response => dispatch(cargandoProyectosSuccess(response.data)))
  }
}

export function guardarProyecto(data) {
  return function(dispatch) {

    axios.post('http://localhost/analisismedios/backend/api/proyecto/', data)
      .then(response => {
        dispatch(guardandoProyectoSuccess({
          id_proyecto: response.data.idProyecto,
          nombre_corto: data.nombreCorto,
          descripcion: data.descripcion
        }))
      });

  }
}
