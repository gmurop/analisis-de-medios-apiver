import axios from 'axios';
import Cookies from 'universal-cookie';
import * as UsuarioActionTypes from '../actiontypes/usuario';
import { cargandoProyectosSuccess } from './proyecto';

const cookie = new Cookies();

export const iniciandoSesion = () => {
  return {
    type: UsuarioActionTypes.INICIANDO_SESION,
    iniciandoSesion: true
  }
}

export const iniciandoSesionSuccess = ({data}) => {
  return {
    type: UsuarioActionTypes.INICIANDO_SESION_SUCCESS,
    iniciandoSesion: false,
    redirectToReferrer: true,
    idUsuario: data.id_usuario,
    nombre: data.nombre,
    email: data.email,
  }
}

export const iniciandoSesionError = () => {
  return {
    type: UsuarioActionTypes.INICIANDO_SESION_ERROR,
    iniciandoSesion: false
  }
}

export const validandoTokenSuccess = data => {
  return {
    type: UsuarioActionTypes.VALIDANDO_TOKEN_SUCCESS,
    redirectToReferrer: true,
    idUsuario: data.idUsuario,
    nombre: data.nombre,
    email: data.email
  }
}

export const validandoTokenError = () => {
  return {
    type: UsuarioActionTypes.VALIDANDO_TOKEN_ERROR,
    redirectToReferrer: false
  }
}

export function login(user, password) {
  return function(dispatch) {
    dispatch(iniciandoSesion());

    axios.post(`http://localhost/analisismedios/backend/api/usuario/login/`, {user, password})
      .then(response => {
        setCookie(response.data.token);
        if (response.data.estatus === 'ok') {
          dispatch(iniciandoSesionSuccess(response.data, response.proyectos));
        } else {
          dispatch(iniciandoSesionError());
        }
      })
  }
}

export function validar(token) {
  return function(dispatch) {

    axios.get(`http://localhost/analisismedios/backend/api/usuario/token/${token}`)
      .then(response => {
        if (response.data.estatus === 'ok') {
          dispatch(validandoTokenSuccess(response.data.data));
          dispatch(cargandoProyectosSuccess(response.data.proyectos));
        } else {
          cookie.remove('token');
          dispatch(validandoTokenError());
        }
      })
  }
}

export function logout() {

}

function setCookie(token) {
  cookie.set('token', token, {path: '/'});
}
