import request from 'superagent';
import axios from 'axios';
import Cookies from 'universal-cookie';
const cookie = new Cookies();
/*
  Defaults axios
*/

axios.defaults.headers.common['Authorization'] = cookie.get('token');

import * as NotaActionTypes from '../actiontypes/nota';

export const agregandoNota = () => {
  return {
    type: NotaActionTypes.AGREGANDO_NOTA,
    agregandoNota: true
  };
};

export const agregandoNotaSuccess = notasAgregadas => {
  return {
    type: NotaActionTypes.AGREGANDO_NOTA_SUCCESS,
    agregandoNota: false,
    notasAgregadas
  };
};

export const cargandoNotas = () => {
  return {
    type: NotaActionTypes.CARGANDO_NOTAS,
    cargandoNotas: true
  }
}

export const cargandoNotasSuccess = (response) => {
  return {
    type: NotaActionTypes.CARGANDO_NOTAS_SUCCESS,
    cargandoNotas: false,
    notas: response.data.notas
  }
}

export const cargandoNotasError = (error) => {
  return {
    type: NotaActionTypes.CARGANDO_NOTAS_ERROR,
    cargandoNotas: false
  }
};

export const toggleSidebar = (toggle) => {
  return {
    type: NotaActionTypes.TOGGLE_DETALLES_NOTA,
    toggleSidebar: toggle
  }
}

export const verDetallesNota = (index) => {
  return {
    type: NotaActionTypes.VER_DETALLES_NOTA,
    toggleSidebar: true,
    index
  }
}

export const actualizandoNota = () => {
  return {
    type: NotaActionTypes.ACTUALIZANDO_NOTA,
    actualizandoNota: true
  };
};

export const actualizandoNotaSuccess = notaActualizada => {
  return {
    type: NotaActionTypes.ACTUALIZANDO_NOTA_SUCCESS,
    actualizandoNota: false,
    notaActualizada
  };
};

export const actualizandoNotaError = () => {
  return {
    type: NotaActionTypes.ACTUALIZANDO_NOTA_ERROR,
    actualizandoNota: false
  }
}

export const eliminandoNota = () => {
  return {
    type: NotaActionTypes.ELIMINANDO_NOTA,
    eliminandoNota: false
  };
};

export const eliminandoNotaSuccess = () => {
  return {
    type: NotaActionTypes.ELIMINANDO_NOTA_SUCCESS,
    eliminandoNota: false
  };
};

export const agregarMedio = (value) => {
  return {
    type: NotaActionTypes.AGREGAR_MEDIO,
    medio: {text: value, value}
  }
}

export const agregarReportero = (value) => {
  return {
    type: NotaActionTypes.AGREGAR_REPORTERO,
    reportero: {text: value, value}
  }
}

export const setDropzoneRef = ref => {
  return {
    type: NotaActionTypes.SET_DROPZONE_REF,
    dropzoneRef: ref
  }
}

export const cargandoMedios = () => {
  return {
    type: NotaActionTypes.CARGANDO_MEDIOS,
    cargandoMedios: true
  }
}

export const cargandoMediosSuccess = medios => {
  return {
    type: NotaActionTypes.CARGANDO_MEDIOS_SUCCESS,
    cargandoMedios: false,
    medios
  }
}

export const cargandoMediosError = () => {
  return {
    type: NotaActionTypes.CARGANDO_MEDIOS_ERROR,
    cargandoMedios: false
  }
}

export const cargandoReporteros = () => {
  return {
    type: NotaActionTypes.CARGANDO_REPORTEROS,
    cargandoReporteros: true
  }
}

export const cargandoReporterosSuccess = reporteros => {
  return {
    type: NotaActionTypes.CARGANDO_REPORTEROS_SUCCESS,
    cargandoReporteros: false,
    reporteros
  }
}

export const cargandoReporterosError = () => {
  return {
    type: NotaActionTypes.CARGANDO_REPORTEROS_ERROR,
    cargandoReporteros: false
  }
}

export const cargandoTemas = () => {
  return {
    type: NotaActionTypes.CARGANDO_TEMAS,
    cargandoTemas: true
  }
}

export const cargandoTemasSuccess = temas => {
  return {
    type: NotaActionTypes.CARGANDO_TEMAS_SUCCESS,
    temas,
    cargandoTemas: false
  }
}

export const cargandoTemasError = () => {
  return {
    type: NotaActionTypes.CARGANDO_TEMAS_ERROR,
    cargandoTemas: false
  }
}

export const agregandoTema = () => {
  return {
    type: NotaActionTypes.AGREGANDO_TEMA,
    agregandoTema: true
  }
}

export const agregandoTemaSuccess = tema => {
  return {
    type: NotaActionTypes.AGREGANDO_TEMA_SUCCESS,
    agregandoTema: false,
    tema
  }
}

export const agregandoTemaError = () => {
  return {
    type: NotaActionTypes.AGREGANDO_TEMA_ERROR,
    agregandoTema: false
  }
}

/**
  Acciones asíncronas:
*/

export function cargarNotas(idProyecto = 0) {
  return function(dispatch) {
    //Cargando notas
    dispatch(cargandoNotas());

    axios.get(`http://localhost/analisismedios/backend/api/nota/${idProyecto}`)
            .then(function(response) {
                dispatch(cargandoNotasSuccess(response));
            })
            .catch(error => {
               dispatch(cargandoNotasError(error));
            });
  }
}

export function onDrop(idProyecto, files) {

  return function(dispatch) {
    //TODO: Convertir esta función de superagent a axios

    dispatch(agregandoNota());

    var req = request.post(`http://localhost/analisismedios/backend/api/nota/${idProyecto}`);
    files.forEach(file => req.attach(file.name, file));
    req.on('progress', e => {
        console.log(e.direction, "is done", e.percent, "%");
    })
    .end(function(err, res) {
        if (res.ok) {
          dispatch(agregandoNotaSuccess(res.body.notas));
            //thiss.props.loadNotas();
            //TODO: Agregar las notas a state, no cargarlas de nuevo
        }
    });
  }

}

export function actualizarNota(values) {

  return function (dispatch) {
    dispatch(actualizandoNota());

    axios.put(`http://localhost/analisismedios/backend/api/nota/${values.id_nota}`, values)
            .then(response => {
              dispatch(actualizandoNotaSuccess(values));
            })
            .catch(error => {
              console.error(error);
              dispatch(actualizandoNotaError());
            });
  }
}

export function eliminarNota(idNota) {

  return function(dispatch) {
    dispatch(eliminandoNota());

    axios.delete(`http://localhost/analisismedios/backend/api/nota/${idNota}`)
    .then(response => {
      dispatch(eliminandoNotaSuccess());
      dispatch(toggleSidebar(true));
    })
    .catch(error => {
      dispatch(eliminandoNotaError());
    });
  }

}

export function cargarMedios() {
  return function(dispatch) {
    dispatch(cargandoMedios());
    axios.get('http://localhost/analisismedios/backend/api/medios', {
      transformResponse: [medios => medios.map(medio => ({key: medio.id_medio, text: medio.nombre, value: medio.nombre}))],
      responseType: 'json'
    })
      .then(response => {
        dispatch(cargandoMediosSuccess(response.data));
      })
      .catch(error => {
        console.error(error);
        dispatch(cargandoMediosError());
      });
  }
}

export function cargarReporteros() {

  return function(dispatch) {
    dispatch(cargandoReporteros());

    axios.get('http://localhost/analisismedios/backend/api/reporteros', {
      transformResponse: [data => data.map(reportero => ({key: reportero.id_reportero, text: reportero.nombre, value: reportero.nombre}))],
      responseType: 'json'
    })
      .then(response => {
        dispatch(cargandoReporterosSuccess(response.data));
      })
      .catch(error => {
        dispatch(cargandoReporterosError());
      });
  }

}

export function cargarTemas() {

  return function(dispatch) {
    dispatch(cargandoTemas());

    axios.get('http://localhost/analisismedios/backend/api/temas', {
      transformResponse: [data => data.map(tema => ({key: tema.id_tema, text: tema.descripcion, value: tema.id_tema, color: tema.color}))],
      responseType: 'json'
    })
      .then(response => {
        dispatch(cargandoTemasSuccess(response.data))
      })
      .catch(error => {
        dispatch(cargandoTemasError());
      })
  }

}

export function cargarPersonajes(idNota) {

  return function(dispatch) {
    dispatch(cargandoPersonajes());

    axios.get(`http://localhost/analisismedios/backend/api/nota/${idNota}/personajes`, {
      transformResponse: [data => data.map(personaje => ({key: personaje.id_personaje, text: personaje.nombre, value: personaje.id_personaje}))],
      responseType: 'json'
    })
    .then(response => {
      dispatch(cargandoPersonajesSuccess(response.data))
    })
    .catch(error => {
      dispatch(cargandoPersonajesError())
    })
  }

}

export function agregarTema(values) {
  return function(dispatch) {
    dispatch(agregandoTema());
    axios.post('http://localhost/analisismedios/backend/api/tema/extracto', values)
    .then(response => {
      dispatch(agregandoTemaSuccess(values));
    })
    .catch(error => {
      console.error(error);
      dispatch(agregandoTemaError());
    })

  }
}
