<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Proyecto_model extends CI_Model {

    public function __construct() {

    }

    public function obtener_proyectos($idUsuario = null) {

      if (!empty($idUsuario)) {
        $this->db->join('administracion_proyecto ap', 'ap.fk_id_proyecto = p.id_proyecto')
                ->where('ap.fk_id_usuario', $idUsuario);

      }

        $query = $this->db->select('*')
                ->from('proyecto p')
                ->where('estatus >= 0')//-1 es eliminada
                ->get();

        return $query->result_array();
    }

    public function guardar_proyecto($id_usuario, $nombre_corto, $descripcion) {
      $fecha = date('Y-m-d');

      $this->db->trans_start();

      $this->db->insert('proyecto', [
        'nombre_corto' => $nombre_corto,
        'descripcion' => $descripcion,
        'fk_id_usuario_creacion' => $id_usuario,
        'fecha' => $fecha
      ]);

      $id_proyecto = $this->db->insert_id();

      $this->db->insert('administracion_proyecto', [
        'fk_id_proyecto' => $id_proyecto,
        'fk_id_usuario' => $id_usuario,
        'perfil' => 1
      ]);

      $this->db->trans_complete();

      return ($this->db->trans_status() === true) ? $id_proyecto : false;
    }

}
