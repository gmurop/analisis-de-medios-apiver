<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Nota_model extends CI_Model {

    public function __construct() {

    }

    public function obtener_nota($id_proyecto = NULL, $id_nota = NULL) {

      if (!empty($id_proyecto)) {
        $this->db->where('nota.fk_id_proyecto', $id_proyecto);
      }

      if (!empty($id_nota)) {
        $this->db->where('nota.id_nota', $id_nota);
      }

        $query = $this->db->select('nota.*, reportero.nombre as reportero, medio.nombre as medio')
                ->from('nota')
                ->join('reportero', 'nota.fk_id_reportero_firmo = reportero.id_reportero', 'left')
                ->join('medio', 'nota.fk_id_medio_emitio = medio.id_medio', 'left')
                ->where('estatus >= 0')//-1 es eliminada
                ->order_by('nota.estatus', 'DESC')
                ->order_by('nota.id_nota')
                ->get();

        return (!empty($id_nota)) ? $query->row() : $query->result_array();
    }

    public function guardar_nota($id_proyecto, $nombre_archivo) {
        $this->db->insert('nota', [
            'fk_id_proyecto' => $id_proyecto,
            'nombre_archivo' => $nombre_archivo
        ]);

        return $this->db->insert_id();
    }

    public function obtener_temas($id_nota) {
        $query = $this->db->select('nt.fk_id_nota as id_nota, t.id_tema, nt.extracto')
                ->from('nota_tema nt')
                ->join('tema t', 'nt.fk_id_tema = t.id_tema')
                ->where('nt.fk_id_nota', $id_nota)
                ->get();

        return $query->result();
    }

    public function actualizar_nota($id_nota, $encabezado, $url, $fecha, $medio, $reportero, $tendencia) {
      $id_medio = $this->insertar_medio($medio);
      $id_reportero = $this->insertar_reportero($reportero);

      $actualizar = $this->db->where('id_nota', $id_nota)
        ->update('nota', [
          'encabezado'  => $encabezado,
          'fecha'       => $fecha,
          'url_nota'    => $url,
          'tendencia'   => $tendencia,
          'fk_id_medio_emitio' => $id_medio,
          'fk_id_reportero_firmo' => $id_reportero,
          'estatus' => 1
        ]);

        return $actualizar;
    }

    private function insertar_medio($medio) {
      $query = $this->db->get_where('medio', ['nombre' => $medio]);

      if ($query->num_rows() === 0) {
        $insertar = $this->db->insert('medio', ['nombre' => $medio]);
        $id = $this->db->insert_id();
      } else {
        $id = $query->row('id_medio');
      }

      return $id;
    }

    private function insertar_reportero($reportero) {
      $query = $this->db->get_where('reportero', ['nombre' => $reportero]);

      if ($query->num_rows() === 0) {
        $insertar = $this->db->insert('reportero', ['nombre' => $reportero]);
        $id = $this->db->insert_id();
      } else {
        $id = $query->row('id_reportero');
      }

      return $id;
    }

    public function eliminar_nota($id_nota) {
      return $this->db->where('id_nota', $id_nota)->update('nota', ['estatus' => -1]);
    }

    public function cat_medios() {
      return $this->db->get('medio')->result();
    }

    public function cat_reporteros() {
      return $this->db->get('reportero')->result();
    }


    public function cat_temas() {
        return $this->db->get('tema')->result();
    }

    public function agregar_extracto_tema($id_nota, $id_tema, $extracto) {
      $agregar = $this->db->insert('nota_tema', [
        'fk_id_nota' => $id_nota,
        'fk_id_tema' => $id_tema,
        'extracto' => $extracto
      ]);

      return $agregar;
    }

    public function eliminar_extracto_tema($id_nota, $id_tema) {
      $eliminar = $this->db->where([
        'fk_id_nota' => $id_nota,
        'fk_id_tema' => $id_tema
        ])->delete('nota_tema');

      return $eliminar;
    }

    public function obtener_personajes($id_nota) {
      $query = $this->db->select('*')
                ->from('nota_personaje np')
                ->join('nota n', 'np.fk_id_nota = n.id_nota')
                ->join('personaje p', 'np.fk_id_personaje = p.id_personaje')
                ->where('fk_id_nota', $id_nota)
                ->get();

      return $query->result();
    }

    public function agregar_nota_personaje($id_nota, $personaje, $organizacion) {
      $buscar_organizacion = $this->buscar_organizacion($organizacion);
      $buscar_personaje = $this->buscar_personaje($personaje);

      $id_organizacion = (!(bool) $buscar_organizacion) ?
        $this->agregar_organizacion($organizacion) :
        $buscar_organizacion->id_organizacion;

      $id_personaje = (!(bool) $buscar_personaje) ?
        $this->agregar_personaje($personaje) :
        $buscar_personaje->id_personaje;

      $agregar_nota_personaje = $this->insert('nota_personaje', [
        'fk_id_nota' => $id_nota,
        'fk_id_personaje' => $id_personaje
      ]);

      return $agregar_nota_personaje;
    }

    private function buscar_organizacion($organizacion) {
      $query = $this->db->get_Where('organizacion', "nombre = '$organizacion'");
      return $query->row();
    }

    private function agregar_organizacion($organizacion) {
      $this->db->insert('organizacion', ['nombre' => $organizacion]);
      return $this->db->insert_id();
    }

    public function buscar_personaje($personaje) {
      $query = $this->db->get_where('personaje', "nombre = '$personaje'");
      return $query->row();
    }

    private function agregar_personaje($personaje) {
      $this->db->insert('personaje', ['nombre' => $personaje]);
      return $this->db->insert_id();
    }

    public function eliminar_personaje_nota($id_nota, $id_personaje) {
      $eliminar = $this->db->where([
        'fk_id_nota' => $id_nota,
        'fk_id_personaje' => $id_personaje
      ])->delete('nota_personaje');

      return $eliminar;
    }

    public function obtener_conflictos($id_nota) {
      $query = $this->db->select('*')
                ->from('nota_conflicto nc')
                ->where('fk_id_nota', $id_nota)
                ->get();

      return $query->result();
    }

    public function agregar_conflicto($id_nota, $titulo, $descripcion) {
      $insertar = $this->db->insert('nota_conflicto', [
        'fk_id_nota' => $id_nota,
        'titulo' => $titulo,
        'descripcion' => $descripcion
      ]);

      return $insertar;
    }

    public function eliminar_conflicto($id_nota, $id_conflicto) {
      $eliminar = $this->db->where([
          'id_conflicto' => $id_conflicto,
          'fk_id_nota' => $id_nota
        ])->delete('nota_conflicto');

      return $eliminar;
    }

}
