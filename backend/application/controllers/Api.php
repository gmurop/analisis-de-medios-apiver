<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . "./libraries/REST_Controller.php");

use Restserver\Libraries\REST_Controller;
use Firebase\JWT\JWT;

class Api extends REST_Controller {

    public $token = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('nota_model');
        $this->load->model('proyecto_model');
        $this->load->helper('form', 'url');
    }

    public function login_post() {

        $json = file_get_contents('php://input');

        $obj = json_decode($json);

        $email = $obj->user;
        $password = $obj->password;

        $this->load->model('usuario_model');

        $iniciar_sesion = $this->usuario_model->login($email, $password);

        if ($iniciar_sesion['estatus'] === true) {

            $data = $iniciar_sesion['data'];

            //Se cargan los proyectos asignados al usuario
            $proyectos = $this->proyecto_model->obtener_proyectos($data->id_usuario);

            $token = [
              'idUsuario' => $data->id_usuario,
              'nombre' => $data->nombre,
              'email' => $data->email
            ];

            $jwt = $this->jwt->encode($token, $this->config->item('jwt_secret'));

            $this->response([
              'token' => $jwt,
              'estatus' => 'ok',
              'mensaje' => 'Acceso correcto',
              'data' => $iniciar_sesion['data']
              ,
              'proyectos' => $proyectos
            ]);

        } else {
            $this->response([
                'estatus' => 'error',
                'mensaje' => 'El nombre de usuario o contraseña es incorrecto.'
            ]);
        }

    }

    public function index_get() {
        $this->response("Bienvenido a la API de Análisis de Medios ;)");
    }

    public function nota_get($id_proyecto = NULL) {
        $notas = $this->nota_model->obtener_nota($id_proyecto);

        //Se consultan los temas
        foreach ($notas as $key=>$nota) {
            $temas = $this->nota_model->obtener_temas($nota['id_nota']);
            $notas[$key]['temas'] = $temas;
        }

        $response = [
            'notas' => $notas,
            'numeralia' => [
                'notas_pendientes' => 0
            ]
        ];

        $this->response($response);
    }

    public function nota_post($id_proyecto) {

        $this->data['notas'] = [];

        $config = [
            'upload_path'       => './notas/',
            'allowed_types'     => 'jpg|pdf',
            'encrypt_name'      => TRUE,
            'file_ext_tolower'  => TRUE
        ];

        $this->load->library('upload', $config);

        foreach($_FILES as $key=>$nota) {
            if (!$this->upload->do_upload($key)) {
                $this->data['error'] = $this->upload->display_errors();
            } else {
                $nombre_nota = $this->upload->data('file_name');

                $id_nota = $this->nota_model->guardar_nota($id_proyecto, $nombre_nota);

                if ((bool) $id_nota) {
                $nota = (array) $this->nota_model->obtener_nota($id_proyecto, $id_nota);
                $nota['temas'] = [];

                $this->data['notas'][] = $nota;
                } else {
                    $this->response(['error' => "Error al guardar la nota."]);
                }
            }
        }

        $this->response($this->data);
    }

    public function nota_put($id_nota) {
        $encabezado = $this->put('encabezado');
        $url = $this->put('url_nota');
        $fecha = $this->put('fecha');
        $medio = $this->put('medio');
        $reportero = $this->put('reportero');
        $tendencia = $this->put('tendencia');

        $actualizar_nota = $this->nota_model->actualizar_nota($id_nota, $encabezado, $url, $fecha, $medio, $reportero, $tendencia);

        $this->response($actualizar_nota);
    }

    public function nota_delete($id_nota) {
      $eliminar = $this->nota_model->eliminar_nota($id_nota);

      $this->response($eliminar);
    }

    public function proyecto_get($id_usuario) {
      $proyectos = $this->proyecto_model->obtener_proyectos($id_usuario);

      $this->response($proyectos);
    }

    public function proyecto_post() {

      $token = $_SERVER['HTTP_AUTHORIZATION'];
      $jwt = $this->jwt->decode($token, $this->config->item('jwt_secret'));

      $json = file_get_contents('php://input');
      $obj = json_decode($json);

      $id_usuario = $jwt->idUsuario;
      $nombre_corto = $obj->nombreCorto;
      $descripcion = $obj->descripcion;

      //Regresa el id del proyecto
      $id_proyecto = $this->proyecto_model->guardar_proyecto($id_usuario, $nombre_corto, $descripcion);

      if ((bool) $id_proyecto) {
        $data = [
          'estatus' => 'ok',
          'idProyecto' => $id_proyecto
        ];

      } else {
        $data = [
          'estatus' => 'error',
          'idProyecto' => null
        ];
      }

      $this->response($data);

    }

    public function validar_get($token) {

      if ($token === 'undefined') {
        $this->response([
          'estatus' => 'error',
          'data' => []
        ]);
        return;
      }

      $jwt = $this->jwt->decode($token, $this->config->item('jwt_secret'), false);

      if (!empty($jwt)) {
        $proyectos = $this->proyecto_model->obtener_proyectos($jwt->idUsuario);

        $data = [
            'estatus' => 'ok',
            'data' => $jwt,
            'proyectos' => $proyectos
          ];
      } else {
        $data = [
          'estatus' => 'error',
          'data' => []
        ];
      }

      $this->response($data);

    }

    public function medios_get() {
      $medios = $this->nota_model->cat_medios();
      $this->response($medios);
    }

    public function reporteros_get() {
      $reporteros = $this->nota_model->cat_reporteros();
      $this->response($reporteros);
    }

    public function temas_get() {
        $temas = $this->nota_model->cat_temas();
        $this->response($temas);
    }

    public function nota_personajes_get($id_nota) {
      $personajes = $this->nota_model->obtener_personajes($id_nota);
      $this->response($personajes);
    }

    public function tema_post() {
      $json = file_get_contents('php://input');
      $input = json_decode($json);

      $id_nota = $input->id_nota;
      $id_tema = $input->id_tema;
      $extracto = $input->extracto;

      $agregar_extracto_tema = $this->nota_model->agregar_extracto_tema($id_nota, $id_tema, $extracto);

      if ((bool) $agregar_extracto_tema) {
        $this->data = [
          'estatus' => true
        ];
      } else {
        $this->data = [
          'estatus' => false
        ];
      }

      $this->response($this->data);
    }

    public function tema_delete($id_nota, $id_tema) {
      $eliminar = $this->nota_model->eliminar_tema($id_nota, $id_tema);

      if ((bool) $eliminar) {
        $this->data = [
          'estatus' => true
        ];
      } else {
        $this->data = [
          'estatus' => false
        ];
      }

      $this->response($this->data);
    }

    public function personaje_post() {
      $json = file_get_contents('php://input');
      $input = json_decode($json);

      $id_nota = $input->id_nota;
      $personaje = $input->personaje;
      $organizacion = $input->organizacion;

      $agregar_personaje = $this->nota_model->agregar_nota_personaje($id_nota, $personaje, $organizacion);

      if ((bool) $agregar_personaje) {
        $this->data = [
          'estatus' => true
        ];
      } else {
        $this->data = [
          'estatus' => false
        ];
      }

      $this->response($this->data);
    }

    public function personaje_delete($id_nota, $id_personaje) {
      $eliminar = $this->nota_model->eliminar_personaje_nota($id_nota, $id_personaje);

      $this->response([
        'estatus' => $eliminar
      ]);
    }

    public function nota_conflictos_get($id_nota) {
      $conflictos = $this->nota_model->obtener_conflictos($id_nota);
      $this->response($conflictos);
    }

    public function conflicto_post() {
      $json = file_get_contents('php://input');
      $input = json_decode($json);

      $id_nota = $input->id_nota;
      $titulo = $input->titulo;
      $descripcion = $input->descripcion;

      $agregar_conflicto = $this->nota_model->agregar_conflicto($id_nota, $titulo, $descripcion);

      if ((bool) $agregar_conflicto) {
        $this->data = [
          'estatus' => true
        ];
      } else {
        $this->data = [
          'estatus' => false
        ];
      }

      $this->response($this->data);
    }

    public function conflicto_delete($id_nota, $id_conflicto) {
      $eliminar = $this->nota_model->eliminar_personaje_nota($id_nota, $id_personaje);

      $this->response([
        'estatus' => $eliminar
      ]);
    }

}
